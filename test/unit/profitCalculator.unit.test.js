'use strict';

const chai = require('chai');
const expect = chai.expect;

const profitCalculator = require('../../app/calculators/profitCalculator');

describe('# profitCalculator module', () => {
  context('when empty arguments', () => {
    context('when both array are undefined', () => {
      it('should return empty array', () => {
        const result = profitCalculator.calculateProfit();
        expect(result).to.be.an('array').that.have.lengthOf(0);
      });
    });

    context('when both array are empty', () => {
      it('should return empty array', () => {
        const result = profitCalculator.calculateProfit([], []);
        expect(result).to.be.an('array').that.have.lengthOf(0);
      });
    });

    context('when asks array is empty', () => {
      it('should return empty array', () => {
        const result = profitCalculator.calculateProfit([], [{ amount: 1, rate: 10000 }]);
        expect(result).to.be.an('array').that.have.lengthOf(0);
      });
    });

    context('when bids array is empty', () => {
      it('should return empty array', () => {
        const result = profitCalculator.calculateProfit([{ amount: 1, rate: 10000 }], []);
        expect(result).to.be.an('array').that.have.lengthOf(0);
      });
    });
  });

  context('when existed arguments', () => {
    context('when 1 element and amount of asks is more than amount of bids', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 1, rate: 10000 }];
        const bids = [{ amount: 0.5, rate: 15000 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 0.5, percent: 50 }];
        expect(result).to.deep.equal(expected);
      });
    });

    context('when 1 element and amount of asks is less than amount of bids', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 0.5, rate: 10000 }];
        const bids = [{ amount: 1, rate: 15000 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 0.5, percent: 50 }];
        expect(result).to.deep.equal(expected);
      });
    });

    context('when 1 element and amount of asks is the same as amount of bids', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 1, rate: 10000 }];
        const bids = [{ amount: 1, rate: 15000 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 1, percent: 50 }];
        expect(result).to.deep.equal(expected);
      });
    });

    context('when 1 asks and 2 bids - 1 deal', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 0.5, rate: 10000 }];
        const bids = [{ amount: 0.5, rate: 15000 }, { amount: 0.5, rate: 15000 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 0.5, percent: 50 }];
        expect(result).to.deep.equal(expected);
      });
    });

    context('when 1 asks and 2 bids - 2 deals', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 1, rate: 10000 }];
        const bids = [{ amount: 0.5, rate: 15000 }, { amount: 0.5, rate: 12500 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 0.5, percent: 50 }, { amount: 0.5, percent: 25 }];
        expect(result).to.deep.equal(expected);
      });
    });

    context('when 2 asks and 2 bids - 2 deal', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 0.5, rate: 5000 }, { amount: 0.5, rate: 10000 }];
        const bids = [{ amount: 0.5, rate: 15000 }, { amount: 0.5, rate: 15000 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 0.5, percent: 200 }, { amount: 0.5, percent: 50 }];
        expect(result).to.deep.equal(expected);
      });
    });

    context('when 2 asks and 1 bids - 2 deals', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 0.5, rate: 5000 }, { amount: 0.5, rate: 10000 }];
        const bids = [{ amount: 1, rate: 15000 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 0.5, percent: 200 }, { amount: 0.5, percent: 50 }];
        expect(result).to.deep.equal(expected);
      });
    });

    context('when some sophisticated logic', () => {
      it('should return correct value', () => {
        const asks = [{ amount: 0.5, rate: 5000 }, { amount: 1, rate: 10000 }];
        const bids = [{ amount: 0.3, rate: 15000 }, { amount: 1.2, rate: 12500 }];
        const result = profitCalculator.calculateProfit(asks, bids);
        const expected = [{ amount: 0.3, percent: 200 }, { amount: 0.2, percent: 150 }, { amount: 1, percent: 25 }];
        expect(result).to.deep.equal(expected);
      });
    });
  });
});