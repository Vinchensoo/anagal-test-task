'use strict';

function calculateProfit(asks = [], bids = []) {
  if (asks.length === 0 || bids.length === 0) {
    return [];
  }

  let asksIndex = 0;
  let bidsIndex = 0;

  let asksTempObject = {};
  let bidsTempObject = {};
  _actualizeObject(asks[0], asksTempObject);
  _actualizeObject(bids[0], bidsTempObject)

  let profit = [];

  while (asksIndex < asks.length && bidsIndex < bids.length) {
    if (asksTempObject.amount === 0) {
      _actualizeObject(asks[asksIndex], asksTempObject);
    }

    if (bidsTempObject.amount === 0) {
      _actualizeObject(bids[bidsIndex], bidsTempObject);
    }

    const amount = Math.min(asksTempObject.amount, bidsTempObject.amount);
    const percent = 100 * (bidsTempObject.rate - asksTempObject.rate) / asksTempObject.rate;
    profit.push({ amount, percent });

    asksTempObject.amount -= amount;
    bidsTempObject.amount -= amount;

    if (asksTempObject.amount === 0) {
      asksIndex++;
    }

    if (bidsTempObject.amount === 0) {
      bidsIndex++;
    }
  }

  return profit;
}

function _actualizeObject(source, destination) {
  destination.rate = source.rate;
  destination.amount = source.amount;
}

module.exports.calculateProfit = calculateProfit;
